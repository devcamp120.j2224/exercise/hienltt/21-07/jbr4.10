package com.devcamp.circlecylinder_api.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.circlecylinder_api.model.*;

@RestController
public class CircleCylinderControl {
    @CrossOrigin
    @GetMapping("circle-area")
    public double getCircleArea(@RequestParam(value = "radius")double radius) {
        Circle newCircle = new Circle(radius);
        return newCircle.getArea();        
    }

    @GetMapping("cylinder-volume")
    public double getCylinderVolume(@RequestParam(value = "radius")double radius, @RequestParam (value="height") double height) {
        Circle newCircle = new Circle(radius);
        return newCircle.getArea()*height;        
    }
}
