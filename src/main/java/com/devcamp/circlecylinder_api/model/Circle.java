package com.devcamp.circlecylinder_api.model;

public class Circle {
    double radius = 1.0;
    String color = "red";

    public double getRadius() {
        return radius;
    }
    public void setRadius(double radius) {
        this.radius = radius;
    }
    
    public String getColor() {
        return color;
    }
    public void setColor(String color) {
        this.color = color;
    }
    public Circle(double radius, String color) {
        this.radius = radius;
        this.color = color;
    }
    public Circle(double radius) {
        this.radius = radius;
    }
    public Circle() {
    }
    public double getArea() {
        return Math.pow(radius, 2) *(Math.PI);
    }
    public String toString() {
        return "Circle[radius= " + radius + ", color = " + color + "]";
    }
}
